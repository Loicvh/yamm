import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM) # Broadcom pin-numbering scheme

pin_sensor = 7 # BCM


GPIO.setup(pin_sensor, GPIO.IN, GPIO.PUD_OFF)



print("Here we go! Press CTRL+C to exit")
try:
    while 1:
        state = GPIO.input(pin_sensor)       # read status of pin/port and assign to variable i   
        print(state)

except KeyboardInterrupt: # If CTRL+C is pressed, exit cleanly:
        GPIO.cleanup() # cleanup all GPIO

